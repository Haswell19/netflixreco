import React, {useEffect, useState} from 'react';
import axios from 'axios';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';
import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import { Container } from '@mui/system';
import { styled } from '@mui/material/styles';
import Card from '@mui/material/Card';
import CardHeader from '@mui/material/CardHeader';
import CardMedia from '@mui/material/CardMedia';
import CardContent from '@mui/material/CardContent';
import Grid from '@mui/material/Grid';
import CardActions from '@mui/material/CardActions';
import Collapse from '@mui/material/Collapse';
import Avatar from '@mui/material/Avatar';
import { red } from '@mui/material/colors';
import FavoriteIcon from '@mui/icons-material/Favorite';
import ShareIcon from '@mui/icons-material/Share';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import MoreVertIcon from '@mui/icons-material/MoreVert';

//Axios Netflix api tool

const options = {
  method: 'GET',
  url: 'https://unogs-unogs-v1.p.rapidapi.com/search/titles',
  params: {limit: '99'},
  headers: {
    'X-RapidAPI-Key': '94efd6c205mshd3fd91f6dea5c8dp1e7ed2jsnc2e4b8415d64',
    'X-RapidAPI-Host': 'unogs-unogs-v1.p.rapidapi.com'
  }
};

export default function App() {
  const [films, getFilms] = useState('');
  String.prototype.replaceJSX = () => 
  {
    return this.split("&#39;").flatMap((item) => [item, "`"]);
  } 
  useEffect(() => {
    getAllFilms();
  }, []);

  const getAllFilms = () => {
    axios.request(options).then(function (response){
      console.log(response);
      var allFilms = response.data.results;
      getFilms(allFilms);
    }).catch(error => console.error('Error: ${error}'));
  }

  return (
    <div style={{ backgroundColor: "#EEEEEE", }}>
      <AppBarMUI/>
      <Container fixed style={{ marginTop: '15px'}}>
      <Grid container spacing={2} rowSpacing={2}>        
          {(() => {
            let td = []
            for(let i = 0; i < films.length; i++)
            {
              td.push(<Grid item xs={4}><Card>
                <CardHeader title={films[i].title.replace("&#39;", "`")} subheader={films[i].title_date}></CardHeader>
                <CardMedia component="img" height="260" image={films[i].img}></CardMedia>
                <CardContent>
                  <Typography variant="body2" color="text.secondary">
                    {films[i].synopsis.replace("&#39;", "'")}
                  </Typography>
                </CardContent>
                </Card></Grid>);
            }
            return td
          })()}
      </Grid>
      </Container>
    </div>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

class AppBarMUI extends React.Component {
  render(){
    return (
      <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static">
        <Toolbar>
          <IconButton
            size="large"
            edge="start"
            color="inherit"
            aria-label="menu"
            sx={{ mr: 2 }}
          >
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
            Netflix Recommendation
          </Typography>
          <Button color="inherit">Login</Button>
        </Toolbar>
      </AppBar>
    </Box>
    );
  }
}

class BodyMUI extends React.Component
{
  render()
  {
    return(
      <Container fixed>

      </Container>
    );
  }
}
